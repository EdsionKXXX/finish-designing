package cn.kwq.pcsystem;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.file.FileReader;
import cn.kwq.pcsystem.blockChain.Block;
import cn.kwq.pcsystem.blockChain.BlockChainUtils;
import cn.kwq.pcsystem.entity.DepartmentDic;
import cn.kwq.pcsystem.entity.MajorDic;
import cn.kwq.pcsystem.entity.User;
import cn.kwq.pcsystem.enums.EnumIntoDB;
import cn.kwq.pcsystem.enums.TranslationEnum;
import cn.kwq.pcsystem.mapper.PermissionMapper;
import cn.kwq.pcsystem.mapper.RoleMapper;
import cn.kwq.pcsystem.pojo.JUser;
import cn.kwq.pcsystem.service.DepartmentDicService;
import cn.kwq.pcsystem.service.MajorDicService;
import cn.kwq.pcsystem.service.UserService;
import cn.kwq.pcsystem.utils.RedisUtils;
import cn.kwq.pcsystem.utils.TranslationUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
class PovertyCertificationSystemApplicationTests {

    @Autowired
    PermissionMapper permissionMapper;
    @Autowired
    RoleMapper roleMapper;
    @Autowired
    UserService userService;

    @Autowired
    DepartmentDicService departmentDicService;

    @Autowired
    MajorDicService majorDicService;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    void contextLoads() throws IOException {

    }

}

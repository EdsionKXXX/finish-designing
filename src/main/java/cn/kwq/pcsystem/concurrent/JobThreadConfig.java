package cn.kwq.pcsystem.concurrent;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.concurrent.ExecutorService;

/**
 * Created with IntelliJ IDEA.
 * 异步工作线程配置类
 * @Author: kwq
 * @Date: 2023/03/06/0:51
 * @Description:
 */
@Getter
@Setter
@Data
@Accessors(chain = true)
public class JobThreadConfig {

    private ExecutorService executorService;

    private Runnable job;

    private String jobName;
    //重试次数 默认3
    private int tryTimes=3;
    //延时 默认0
    private int delay=0;
    //重试次数 默认30s
    private int time=30;


}

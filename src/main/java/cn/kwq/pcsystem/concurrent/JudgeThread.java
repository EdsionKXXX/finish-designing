package cn.kwq.pcsystem.concurrent;

import cn.hutool.core.lang.Assert;
import lombok.Data;

import java.util.concurrent.Callable;

/**
 * Created with IntelliJ IDEA.
 * 监控异步工作线程
 * @Author: kwq
 * @Date: 2023/03/06/0:51
 * @Description:
 */
@Data
public class JudgeThread implements Callable<Boolean> {

    private Runnable job;

    @Override
    public Boolean call()  {
        Assert.isTrue(job!=null,"工作任务不能为空");
        try{
            job.run();
        }catch (Throwable throwable){
            throwable.printStackTrace();
            return false;
        }
        return true;
    }


}

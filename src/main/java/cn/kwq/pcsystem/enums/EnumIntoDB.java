package cn.kwq.pcsystem.enums;

import cn.kwq.pcsystem.entity.DepartmentDic;
import cn.kwq.pcsystem.service.DepartmentDicService;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/14/16:21
 * @Description:
 */
public class EnumIntoDB {

    public static List<DepartmentDic> getDepartEnumMsg() {
        List<DepartmentDic> list =new ArrayList<>();
        for (DepartmentEnum e: DepartmentEnum.values()) {
            DepartmentDic build = DepartmentDic
                    .builder()
                    .departmentName(e.getDepartmentName())
                    .deleted(0)
                    .createdBy(7L)
                    .enumName(e.name())
                    .createdDate(new Date())
                    .departmentId(e.getDepartmentCode())
                    .build();
            System.out.println(build);
            list.add(build);
        }
        return list;
    }
}

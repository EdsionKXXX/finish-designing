package cn.kwq.pcsystem.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum DepartmentEnum implements IEnum<Long> {
    Other_Departments("其它部门",1L),
    Shenzhen_Information_Vocational_Technical_College("深圳信息职业技术学院",2L),
    Intelligent_Manufacturing_Faculty("智能制造学部",3L),
    Economics_Management_Faculty("经济管理学院",4L),
    Political_Science_Law_Faculty("政法学院",5L),
    Marxism_Faculty("马克思主义学院",6L),
    Literature_Faculty("文学院",7L),
    Foreign_Faculty("外国语学院",8L),
    Math_Arithmetic_Faculty("数学与计算科信学院",9L),
    Applied_Physics_Materials_Faculty("应用物理与材料学院",10L),
    Civil_Engineering_Architecture_Faculty("土木建筑学院",11L),
    Biotechnology_Health_Faculty("生物科技与大健康学院",12L),
    Textile_Materials_Engineering_Faculty("纺织材料与工程学院",13L),
    Rail_Transit_Faculty("轨道交通学院",14L),
    Art_Design_Faculty("艺术设计学院",15L),
    General_Education_Faculty("通识教育学院",16L),
    Sports_Ministry("体育部",17L),
    Guangdong_Overseas_Chinese_Culture_Research_Institute("广东侨乡文化研究院",18L),
    Innovation_Entrepreneurship_Faculty("创新创业学院",19L),
    Further_Education_Faculty("继续教育学院",20L),
    Students_Division("学生工作部",21L),
    Veterans_Service_Center("退役军人服务中心",22L),
    Aesthetic_Education_Center("美育教育中心",23L),
    Psycho_Educational_Service_Center("心理教育服务中心",24L),
    Military_Department("军事教研室",25L),
    Career_Center("就业指导中心",26L),
    China_German_Institute_Artificial_Intelligence("中德江门人工智能研究院",27L),
    Library("图书馆",28L),

    Clinics("医务所",29L),
    Modern_Educational_Technology_Center("现代教育技术中心",30L),

    Unlimited("不限",31L);

    private String departmentName;
    private Long departmentCode;
    DepartmentEnum(String departmentName, long departmentCode) {
        this.departmentName=departmentName;
        this.departmentCode=departmentCode;
    }

    @Override
    public Long getValue() {
        return this.departmentCode;
    }
    @Override
    public String toString() {
        return this.departmentName;
    }
}

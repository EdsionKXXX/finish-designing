package cn.kwq.pcsystem.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum StuEnum implements IEnum<Integer> {

    STUDENT("学生",0),
    TEACHER("老师",1),
    OTHER("其他",2);

    private String isStudent;
    private Integer code;
    StuEnum(String isStudent, int code) {
        this.code=code;
        this.isStudent=isStudent;
    }

    @Override
    public Integer getValue() {
        return this.code;
    }
    @Override
    public String toString() {
        return this.isStudent;
    }
}

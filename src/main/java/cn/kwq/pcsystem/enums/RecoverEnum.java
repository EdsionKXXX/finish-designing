package cn.kwq.pcsystem.enums;

import lombok.Getter;
import lombok.ToString;

/**
 * Created with IntelliJ IDEA.
 * 账号状态枚举
 * @Author: kwq
 * @Date: 2023/02/28/23:58
 * @Description:
 */
@Getter
@ToString
public enum RecoverEnum {

    RECOVER(0,"恢复账号"),
    INACTIVATION(1,"停用账号");


    private int type;
    private String typeMsg;

    RecoverEnum(int type, String typeMsg) {
        this.type=type;
        this.typeMsg=typeMsg;
    }
}

package cn.kwq.pcsystem.enums;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/14/21:54
 * @Description:
 */

@Getter
public enum TranslationEnum {
    N("n.","名词",""),
    V("v.","动词","表示动作或状态"),
    ADJ("adj.","形容词","用来修饰名词"),
    NUM("num.","数词","表示数目或顺序"),
    ADV("adv.","副词","修饰动、形、副等词，表示动作特征"),
    ART("art.","冠词","用在名词前，帮助说明名词所指的范围"),
    PRON("pron","代词","代替名词、数词、形容词"),
    PREP("prep.","介词","用在名词或代词前，说明它与别的词的关系"),
    CONJ("conj.","连词","表示人或事物的名称"),
    INT("int.","感叹词",""),
    VI("vi.","不及物动词","后不直接带宾语或不带宾语"),
    VT("vt.","及物动词","后必须跟宾语"),
    LONG("long.","长词",""),
    ALL("all.","全部","");

    private String code;
    private String name;
    private String remark;
    TranslationEnum(String code, String name, String remark) {
        this.code=code;
        this.name=name;
        this.remark=remark;
    }

}

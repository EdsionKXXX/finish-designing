package cn.kwq.pcsystem.config;

import cn.kwq.pcsystem.mq.RedisMQListener;
import cn.kwq.pcsystem.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/06/17:01
 * @Description:
 */
@Configuration
public class RedisMQConfig {

    @Autowired
    RedisMQListener redisMQListener;
    /**
     * 注册监听
     * redis消息监听器容器
     * 可以添加多个监听不同通道的redis监听器，只需要把消息监听器和相应的消息处理器绑定
     * @param connectionFactory
     * @return
     */
    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        //订阅了一个叫demoChannel 的通道, 类似redis中的 subscribe 命令
        container.addMessageListener(redisMQListener, new ChannelTopic("testChannel"));
        //阅订匹配test*的通道, 类似redis中 psubscribe 命令(通配符)
        container.addMessageListener(redisMQListener, new PatternTopic("test*"));
        return container;
    }

}

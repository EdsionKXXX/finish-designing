package cn.kwq.pcsystem.config;

import cn.dev33.satoken.stp.StpInterface;
import cn.kwq.pcsystem.mapper.PermissionMapper;
import cn.kwq.pcsystem.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/01/13/0:00
 * @Description:
 */
public class StpAuthenticationConfiguration implements StpInterface {
    @Autowired
    PermissionMapper permissionMapper;
    @Autowired
    RoleMapper roleMapper;

    @Override
    public List<String> getPermissionList(Object loginId, String s) {
       return  permissionMapper.findAllPermission((Long) loginId);
    }

    @Override
    public List<String> getRoleList(Object loginId, String s) {
        return roleMapper.findAllRole((Long) loginId);
    }
}

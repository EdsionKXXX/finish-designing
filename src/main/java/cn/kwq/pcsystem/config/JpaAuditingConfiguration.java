package cn.kwq.pcsystem.config;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.dev33.satoken.stp.StpUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;


import java.util.Optional;

/**
 * 开启审计的配置类
 */
@Configuration
public class JpaAuditingConfiguration implements AuditorAware<Long>{

    @NotNull
    @Override
    public Optional<Long> getCurrentAuditor() {
        //DONE:  2022/11/4 0:54 获取当前操作id，交给jpa审计
        //获取当前会话账号id, 并转化为`long`类型
        long loginId = StpUtil.getLoginIdAsLong();
        return Optional.of(loginId);
    }

}
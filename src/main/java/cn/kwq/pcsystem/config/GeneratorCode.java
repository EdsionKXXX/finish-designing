package cn.kwq.pcsystem.config;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Arrays;
import java.util.Collections;

public class GeneratorCode {
    public static void main(String[] args) {
        final String allTable="family_status_dic";

        String[] split = allTable.split(",");
        Arrays.stream(split).forEach(item->{
            System.out.println("[Doing]"+item);
            FastAutoGenerator.create("jdbc:mysql://localhost:3306/poverty_certification_system?serverTimezone=UTC",
                            "root", "qyft664500")
                    .globalConfig(builder -> {
                        builder.author("kwq") // 设置作者
                                .enableSwagger() // 开启 swagger 模式
                                .fileOverride() // 覆盖已生成文件
                                .dateType(DateType.ONLY_DATE)//时间类型
                                .outputDir("D:\\CodeSpace\\Project\\PovertyCertificationSystem\\src\\main\\java"); // 指定输出目录
                    })
                    .packageConfig(builder -> {
                        builder.parent("cn.kwq.pcsystem") // 设置父包名
                                .pathInfo(Collections.singletonMap(OutputFile.xml, "D:\\CodeSpace\\Project\\PovertyCertificationSystem\\src\\main\\resources\\mapper")); // 设置mapperXml生成路径
                    })
                    .strategyConfig(builder -> {
                        builder.addInclude(item)// 设置需要生成的表名
                                .entityBuilder()//配置对象
                                .enableLombok()//lombok模式
                                .build()
                                .controllerBuilder()
                                .enableRestStyle()//开启res controller
                                .build()
                                .mapperBuilder()
                                .enableBaseResultMap()//启用 BaseResultMap 生成
                                .enableMapperAnnotation()//开启@mapper注解
                                .build()
                                .serviceBuilder()
                                .formatServiceFileName("%sService")
                                .formatServiceImplFileName("%sServiceImp")
                                .build();
                    })
                    .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                    .execute();
        });



    }
}

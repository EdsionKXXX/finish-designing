package cn.kwq.pcsystem.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaIgnore;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.lang.Validator;
import cn.kwq.pcsystem.common.Result;
import cn.kwq.pcsystem.common.ResultCode;
import cn.kwq.pcsystem.concurrent.ConcurrentJobThreadUtils;
import cn.kwq.pcsystem.concurrent.JobThreadConfig;
import cn.kwq.pcsystem.dto.*;
import cn.kwq.pcsystem.entity.User;
import cn.kwq.pcsystem.enums.RecoverEnum;
import cn.kwq.pcsystem.excel.SquadListener;
import cn.kwq.pcsystem.exception.CaptchaException;
import cn.kwq.pcsystem.exception.UserExistException;
import cn.kwq.pcsystem.exception.UserFormatException;
import cn.kwq.pcsystem.service.UserService;
import cn.kwq.pcsystem.utils.CaptchaUtils;
import cn.kwq.pcsystem.utils.ExcelUtils;
import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/01/09/21:25
 * @Description:
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @SaIgnore//可以匿名访问该接口
    @GetMapping("/captcha")
    public Result captcha() throws IOException {
        Map<String, String> map = CaptchaUtils.generateCaptcha();
        return Result
                .success(ResultCode.SUCCESS)
                .data(map);
    }

    @SaIgnore//可以匿名访问该接口
    @PostMapping("/register")
    public Result register(@RequestBody RegisterDTO dto) throws UserExistException, UserFormatException, CaptchaException {
        if(userService.register(dto)){
            return Result.success();
        }
        return Result.error(ResultCode.REGISTER_FAIL);
    }

    @SaIgnore//可以匿名访问该接口
    @PostMapping("/login")
    public Result login(@RequestBody LoginDTO dto) throws UserExistException {
        Long loginId = userService.login(dto);
        StpUtil.login(loginId);
        return Result.success();
    }

    @SaCheckLogin
    @GetMapping("/getMyMsg")
    public Result getMyMsg()  {
        QueryWrapper<User> wrapper=new QueryWrapper<>();
        wrapper.eq("user_id",StpUtil.getLoginIdAsLong());
        User one = userService.getOne(wrapper);
        return Result
                .success()
                .data(one);
    }

    @SaCheckLogin
    @GetMapping("/downloadUserTemple")
    public Result downloadUserTemple(HttpServletResponse response) throws IOException {
        List<RealUserDTO> list=new ArrayList<>();
        boolean success = ExcelUtils
                .writeExcel(RealUserDTO.class, list, "模板", response, "五邑大学困难鉴定系统人员录入模板");
        if (success){
            return Result.success();
        }
        return Result.error();
    }
    @SaIgnore//可以匿名访问该接口
    @GetMapping("/importSquadExcel")
    public Result importSquadExcel(@RequestPart(value = "file") MultipartFile file) throws IOException {
        List<RealUserDTO> list = EasyExcel
                .read(file.getInputStream())
                .head(RealUserDTO.class)
                .sheet()
                .doReadSync();
        list.forEach(item->{
           userService.createUserNotThroughRegister(item);
        });
        return Result.success();
    }


    @PostMapping("/findUser")
    public Result findUser(@RequestBody Map<String,String> map){
        String msg = map.get("key");
        QueryWrapper<User> wrapper=new QueryWrapper<>();
        if(Validator.isEmail(msg)) {
            wrapper.eq("email",msg);
        }else if (Validator.isMobile(msg)) {
            wrapper.eq("phone",msg);
        }else {
            wrapper.like("user_name",msg);
        }
        List<User> list = userService.list(wrapper);
        return Result.success().data(list);
    }

    @PostMapping("/IORUser")
    public Result inactivationOrRecoverUser(@RequestBody IORUserDTO dto){
       if ("Recover".equals(dto.getType())){
           userService.inactivationOrRecoverUser(RecoverEnum.RECOVER,dto.getUserId());
       }else {
           userService.inactivationOrRecoverUser(RecoverEnum.INACTIVATION,dto.getUserId());
       }
       return Result.success();
    }

    @PostMapping("/updateUserPsw")
    public Result updateUserPsw(@RequestBody UpdateUserPswDTO dto){
        boolean b = userService.updateUserPsw(dto.getOldPsw(), dto.getNewPsw(), StpUtil.getLoginIdAsLong());
        return Result.success().success(b);
    }

    @ApiOperation("登录接口")
    @SaIgnore//可以匿名访问该接口
    @PostMapping("loginWyuSys")
    public Result LoginWyu(@RequestBody  LoginWyuDTO dto) throws IOException, ParseException {
        Map<String, String> map = userService.loginWyuSys(dto.getToken());
        return Result.success().data(map);
    }


}

package cn.kwq.pcsystem.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaIgnore;
import cn.dev33.satoken.stp.StpUtil;
import cn.kwq.pcsystem.common.Result;
import cn.kwq.pcsystem.entity.User;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/02/28/14:36
 * @Description:
 */
@RestController
@RequestMapping("/dev")
@Api("开发者专用接口")
public class DevController {
    @SaIgnore
    @GetMapping("/getToken")
    public Result getToken()  {
        StpUtil.login(7L);
        return Result
                .success()
                .data(StpUtil.getTokenInfo());
    }
}

package cn.kwq.pcsystem.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kwq
 * @since 2023-03-12
 */
@RestController
@RequestMapping("/realUser")
public class RealUserController {

}

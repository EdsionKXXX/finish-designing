package cn.kwq.pcsystem.exception;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/19/21:13
 * @Description:
 */
public class BlockChainError extends Error{

    public BlockChainError() {
        super();
    }

    public BlockChainError(String message) {
        super(message);
    }
}

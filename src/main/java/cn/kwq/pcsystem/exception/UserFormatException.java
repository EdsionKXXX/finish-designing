package cn.kwq.pcsystem.exception;

/**
 * Created with IntelliJ IDEA.
 * 用户格式错误
 * @Author: kwq
 * @Date: 2023/02/04/16:26
 * @Description:
 */
public class UserFormatException extends Exception {
    public UserFormatException() {
        super();
    }
    public UserFormatException(String str) {
        super(str);
    }
}

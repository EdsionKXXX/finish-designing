package cn.kwq.pcsystem.exception;

/**
 * Created with IntelliJ IDEA.
 * 用户不存在错误
 * @Author: kwq
 * @Date: 2023/02/04/15:34
 * @Description:
 */
public class UserExistException extends Exception{

    public UserExistException() {
        super();
    }
    public UserExistException(String str) {
        super(str);
    }
}

package cn.kwq.pcsystem.exception;

/**
 * Created with IntelliJ IDEA.
 * 验证码错误
 * @Author: kwq
 * @Date: 2023/02/28/0:19
 * @Description:
 */
public class CaptchaException extends Exception{
    public CaptchaException() {
        super("验证码错误");
    }
}

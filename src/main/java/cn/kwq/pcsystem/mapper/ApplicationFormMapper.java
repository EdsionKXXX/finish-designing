package cn.kwq.pcsystem.mapper;

import cn.kwq.pcsystem.entity.ApplicationForm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kwq
 * @since 2023-03-23
 */
@Mapper
public interface ApplicationFormMapper extends BaseMapper<ApplicationForm> {

}

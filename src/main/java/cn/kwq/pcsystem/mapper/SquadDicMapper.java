package cn.kwq.pcsystem.mapper;

import cn.kwq.pcsystem.entity.SquadDic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kwq
 * @since 2023-03-12
 */
@Mapper
public interface SquadDicMapper extends BaseMapper<SquadDic> {

}

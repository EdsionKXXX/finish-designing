package cn.kwq.pcsystem.mapper;

import cn.kwq.pcsystem.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kwq
 * @since 2023-01-12
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {

    List<String> findAllPermission(Long userId);

}

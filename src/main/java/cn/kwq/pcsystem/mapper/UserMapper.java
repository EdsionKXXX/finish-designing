package cn.kwq.pcsystem.mapper;

import cn.kwq.pcsystem.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kwq
 * @since 2023-01-09
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}

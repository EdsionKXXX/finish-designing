package cn.kwq.pcsystem.repository;

import cn.kwq.pcsystem.pojo.JSquadDic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface SquadDicRepository extends JpaRepository<JSquadDic, Long> {
}
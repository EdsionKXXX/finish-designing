package cn.kwq.pcsystem.repository;

import cn.kwq.pcsystem.pojo.JUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface UserRepository extends JpaRepository<JUser, Long> {
}
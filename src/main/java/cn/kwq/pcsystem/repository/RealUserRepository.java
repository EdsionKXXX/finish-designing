package cn.kwq.pcsystem.repository;

import cn.kwq.pcsystem.pojo.JRealUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface RealUserRepository extends JpaRepository<JRealUser, Long> {
}
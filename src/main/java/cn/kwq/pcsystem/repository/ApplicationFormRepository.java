package cn.kwq.pcsystem.repository;

import cn.kwq.pcsystem.pojo.JApplicationForm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ApplicationFormRepository extends JpaRepository<JApplicationForm, Long> {
}
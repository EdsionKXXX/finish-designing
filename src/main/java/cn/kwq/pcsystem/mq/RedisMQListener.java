package cn.kwq.pcsystem.mq;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * 监听收到redis推送
 * @Author: kwq
 * @Date: 2023/03/06/17:01
 * @Description:
 */
@Component
public class RedisMQListener implements MessageListener {

    /**
     * 收到订阅消息的操作
     * @param message 收到的消息
     * @param pattern
     */
    @Override
    public void onMessage(@NotNull Message message, byte[] pattern) {

    }
}

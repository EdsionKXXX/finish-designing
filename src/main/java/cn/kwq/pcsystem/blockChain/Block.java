package cn.kwq.pcsystem.blockChain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created with IntelliJ IDEA.
 * 区块链的块
 * @Author: kwq
 * @Date: 2023/03/19/16:44
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
//指定实体类和数据库文档的映射关系    默认实体类名  数据库如果没有该文档，会自动创建
@Document(value="PCS_record_blockChain")
public class Block {
    @Id
    private ObjectId id; //mongoDB推荐使用ID

    //区块链数据
    private String data;
    //上一个链的hash
    private String preHash;
    //自己的hash
    private String hash;
    //用来改变hash的随机数
    private int nonce=1;
}

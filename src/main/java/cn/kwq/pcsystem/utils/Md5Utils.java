package cn.kwq.pcsystem.utils;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import cn.kwq.pcsystem.dto.Md5DTO;
import org.springframework.util.DigestUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/01/04/19:38
 * @Description: md5工具类
 */
public class Md5Utils {

    /*
    用于salt加密的aes密钥
     */
    private final static String AesKey="Sdahsisidhs232t3";

    /**
     * md5加盐加密,生成随机盐
     * @param encrypt 需要加密的明文
     * @return md5加密后密文以及加密后的盐
     */
    public static Md5DTO Md5Encrypt(String encrypt){
        //生成随机盐
        String salt = RandomUtil.randomString(10);
        //构造字符串
        StringBuilder sb = new StringBuilder();
        //盐和加密字符串拼接（加盐）
        sb.append(salt).append(encrypt);
        byte[] bytes = sb.toString().getBytes();
        encrypt = DigestUtils.md5DigestAsHex(bytes);
        Map<String,String> map=new HashMap<>();
        //加密盐
        byte[] byteKey = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue(), AesKey.getBytes()).getEncoded();
        SymmetricCrypto aes = SecureUtil.aes(byteKey);
        salt = aes.encryptBase64(salt);

        return new Md5DTO(encrypt,salt);

    }

    /**
     * md5加密，固定盐
     * @param encrypt 需要加密的明文
     * @param salt 固定的盐
     * @return md5加密后密文
     */
    public static String Md5Encrypt(String encrypt,String salt){
        //解密盐
        byte[] byteKey = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue(), AesKey.getBytes()).getEncoded();
        SymmetricCrypto aes = SecureUtil.aes(byteKey);
        salt = aes.decryptStr(salt);
        //构造字符串
        StringBuilder sb = new StringBuilder();
        //加盐
        sb.append(salt).append(encrypt);
        byte[] bytes = sb.toString().getBytes();
        return DigestUtils.md5DigestAsHex(bytes);
    }

    /**
     * 校验md5正确与否
     * @param cipherStr 密文
     * @param plainStr 明文
     * @param salt 加密后盐
     * @return 返回结果
     */
    public static boolean Md5Validate(String cipherStr,String plainStr,String salt){
        //固定盐加密
        String s = Md5Utils.Md5Encrypt(plainStr, salt);
        //安全比较
        return SafeUtils.safeEqual(cipherStr,s);
    }
}

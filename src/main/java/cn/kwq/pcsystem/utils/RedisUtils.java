package cn.kwq.pcsystem.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/01/05/20:34
 * @Description: redis工具类
 */
@Component
public class RedisUtils {

    //存取的数据是字符串类型
    private static StringRedisTemplate redisStringTemplate;
    //存取对象使用,需要序列化
    private static RedisTemplate<String,Object> redisTemplate;

    // 构造方法注入静态变量
    @Autowired
    public void setRedisStringTemplate(StringRedisTemplate redisStringTemplate,RedisTemplate redisTemplate){
        RedisUtils.redisStringTemplate = redisStringTemplate;
        RedisUtils.redisTemplate=redisTemplate;
    }

    /**
     * 存入string
     * @param key key
     * @param value string的值
     * @param time 时间，单位s
     */
    public static void setStr(String key,String value,Long time){
        if (time>0){
            redisStringTemplate.opsForValue().set(key,value,time,TimeUnit.SECONDS);
        }else {
            redisStringTemplate.opsForValue().set(key,value);

        }

    }

    /**
     * 拿到string
     * @param key key
     * @return 拿到的string
     */
    public static String getStr(String key){
        return redisStringTemplate.opsForValue().get(key);
    }

    /**
     * 左插入,入队用
     * @param key key
     * @param value str
     */
    public static void leftPush(String key,String value){
        redisStringTemplate.opsForList().leftPush(key, value);
    }

    /**
     * 左插入,入队用
     * @param key key
     * @param value obj
     */
    public static void leftPushObj(String key,Object value){
        redisTemplate.opsForList().leftPush(key, value);
    }

    /**
     * 右出队，消费用
     * @param key key
     * @return str
     */
    public static String rightPop(String key){
      return  redisStringTemplate.opsForList().rightPop(key);
    }

    /**
     * 右出队，消费用
     * @param key key
     * @return obj
     */
    public static Object rightPopObj(String key){
        return redisTemplate.opsForList().rightPop(key);
    }

    /**
     * 右入队，消费失败重试入队用
     * @param key key
     * @param value str
     */
    public static void rightPush(String key,String value){
        redisStringTemplate.opsForList().rightPush(key, value);
    }

    /**
     * 右入队，消费失败重试入队用
     * @param key key
     * @param value obj
     */
    public static void rightPushObj(String key,Object value){
        redisTemplate.opsForList().rightPush(key, value);
    }

    /**
     * 发布信息 序列化方式
     * @param channel 管道名
     * @param msg  信息对象
     * @throws JsonProcessingException 序列化错误
     */
    public static void publishObj(String channel,Object msg) throws JsonProcessingException {
        //序列化
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(msg);
        redisStringTemplate.convertAndSend(channel,jsonString);
    }

    /**
     * 发布信息 非序列化方式
     * @param channel 管道名
     * @param msg  信息对象
     */
    public static void publishStr(String channel,String msg) {
        redisStringTemplate.convertAndSend(channel,msg);
    }
}

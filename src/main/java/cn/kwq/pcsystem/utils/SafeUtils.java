package cn.kwq.pcsystem.utils;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/01/04/19:35
 * @Description:
 */
public class SafeUtils {


    /**
     * 安全比较字符串，防计时攻击
     * @param a 比较值1
     * @param b 比较值2
     * @return 返回比较结果
     */
    public static boolean safeEqual(String a, String b) {
        int equal = 0;
        if (a.length() != b.length()) {
            for (int i = 0; i < a.length(); i++) {
                equal |= 1;
            }
            return false;
        }
        for (int i = 0; i < a.length(); i++) {
            equal |= a.charAt(i) ^ b.charAt(i);
        }

        return equal == 0;
    }

}

package cn.kwq.pcsystem.utils;

import cn.kwq.pcsystem.enums.TranslationEnum;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 利用爬虫翻译
 * @Author: kwq
 * @Date: 2023/03/14/21:48
 * @Description:
 */
public class TranslationUtils {

    public static List<String> translationByWangYi(List<String> list, TranslationEnum translationEnum) throws IOException {
            List<String> stringList=new ArrayList<>();

            if ("all.".equals(translationEnum.getCode())){
                list.forEach(item->{
                    String url = "https://www.youdao.com/w/" + item + "/#keyfrom=dict2.top";
                    try {
                        //翻译框
                        Element element = Jsoup.connect(url).get().select("div[class=trans-container]").get(0);
                        String text = element.text();
                        stringList.add(text);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            } else if ("long.".equals(translationEnum.getCode())) {
                list.forEach(item->{
                    String url = "https://www.youdao.com/w/" + item + "/#keyfrom=dict2.top";
                    try {
                        //翻译框
                        Element element = Jsoup.connect(url).get().select("div[class=trans-container]").get(0);
                        Elements wordGroup = element.select("p[class=wordGroup]");
                        Element firstWordGroup = wordGroup.first();
                        String text=null;
                        if (firstWordGroup!=null){
                             text = firstWordGroup.getElementsByTag("a").get(0).text();
                        }else {
                             text = wordGroup.text();
                        }

                        stringList.add(text);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            } else {
                list.forEach(item->{
                    String url = "https://www.youdao.com/w/" + item + "/#keyfrom=dict2.top";
                    try {
                        //翻译框
                        Element element = Jsoup.connect(url).get().select("div[class=trans-container]").get(0);
                        //单词框
                        Elements wordGroup = element.select("p[class=wordGroup]");
                        String translation = null;
                        //遍历获得单行单词内容
                        for (Element e : wordGroup) {
                            //获取到每行 全部字内容
                            Elements span = e.getElementsByTag("span");
                            //获取到词性
                            String text = span.get(0).text();
                            if (translationEnum.getCode().equals(text)){
                                //获取第一个单词内容
                                translation = span.get(1).getElementsByTag("a").get(0).text();
                                break;
                            }
                        }
                        stringList.add(translation);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            }
            return stringList;
    }

    public static String translationByWangYi(String item, TranslationEnum translationEnum) throws IOException {
        String text=null;
        if ("all.".equals(translationEnum.getCode())){
                String url = "https://www.youdao.com/w/" + item + "/#keyfrom=dict2.top";
                try {
                    //翻译框
                    Element element = Jsoup.connect(url).get().select("div[class=trans-container]").get(0);
                    text = element.text();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
        } else if ("long.".equals(translationEnum.getCode())) {

                String url = "https://www.youdao.com/w/" + item + "/#keyfrom=dict2.top";
                try {
                    //翻译框
                    Element element = Jsoup.connect(url).get().select("div[class=trans-container]").get(0);
                    Elements wordGroup = element.select("p[class=wordGroup]");
                    Element firstWordGroup = wordGroup.first();
                    text = firstWordGroup.getElementsByTag("a").get(0).text();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
        } else {
                String url = "https://www.youdao.com/w/" + item + "/#keyfrom=dict2.top";
                try {
                    //翻译框
                    Element element = Jsoup.connect(url).get().select("div[class=trans-container]").get(0);
                    //单词框
                    Elements wordGroup = element.select("p[class=wordGroup]");
                    String translation = null;
                    //遍历获得单行单词内容
                    for (Element e : wordGroup) {
                        //获取到每行 全部字内容
                        Elements span = e.getElementsByTag("span");
                        //获取到词性
                        String text1 = span.get(0).text();
                        if (translationEnum.getCode().equals(text1)){
                            //获取第一个单词内容
                            translation = span.get(1).getElementsByTag("a").get(0).text();
                            break;
                        }
                    }
                    text=translation;
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
        }
        return text;
    }
}

package cn.kwq.pcsystem.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/01/13/0:12
 * @Description: L为link的意思
 */
@Data
@Builder
@Entity //作为hibernate
@Table(name = "role_user")
@NoArgsConstructor
@AllArgsConstructor
public class LRoleUser {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//id自增
    private Long roleUserId;

    private Long roleId;

    private Long userId;

    private int deleted;
}

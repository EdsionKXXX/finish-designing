package cn.kwq.pcsystem.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/12/22:41
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity //作为hibernate
@Table(name = "major_Dic")
@Builder
public class JMajorDic {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//id自增
    @Column(name = "major_id")
    private Long majorId;

    private String majorName;

    private String remark;

    private Long departmentId;

    private int deleted;
}

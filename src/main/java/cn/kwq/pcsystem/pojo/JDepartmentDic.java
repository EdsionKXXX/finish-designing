package cn.kwq.pcsystem.pojo;

import cn.kwq.pcsystem.pojo.Auditable.Auditable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/12/18:06
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity //作为hibernate
@Table(name = "department_Dic")
@Builder
public class JDepartmentDic extends Auditable<Long> {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//id自增
    @Column(name = "department_id")
    private Long departmentId;

    private String departmentName;

    private String remark;

    private String enumName;

    private int deleted;
}

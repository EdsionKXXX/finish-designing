package cn.kwq.pcsystem.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/23/18:11
 * @Description:
 */
@Data
@Builder
@Entity //作为hibernate
@Table(name = "familyStatus_applicationFrom")
@NoArgsConstructor
@AllArgsConstructor
public class LFamilyStatusFrom {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//id自增
    private Long linkId;

    private Long familyStatusId;

    private Long applicationFromId;

    private int deleted;

}

package cn.kwq.pcsystem.pojo;


import cn.kwq.pcsystem.pojo.Auditable.Auditable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity //作为hibernate
@Table(name = "Role")
@Builder
public class JRole extends Auditable<Long> {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//id自增
    @Column(name = "role_id")
    private Long roleId;

    private String role;

    private String remark;

    private String status;

    private int deleted;

}

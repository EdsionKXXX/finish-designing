package cn.kwq.pcsystem.pojo;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/23/18:22
 * @Description:
 */
public class JFamily {

    private Long familyId;

    private Long ApplicationFromId;

    private String familyName;

    private String familyAge;

    private String relation;

    private String familyDepartment;

    private String phone;

    private String job;

    private String cultureStandard;

    private int yearIncome;

    private String health;
}

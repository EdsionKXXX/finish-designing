package cn.kwq.pcsystem.pojo.Auditable;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * jpa审计功能最优解
 *
 *
 * @MappedSuperclass可以让其它子实体类继承相关的字段和属性；
 * @EntityListeners设置监听类，会对新增和修改进行回调处理。
 * @param <U>
 * 子类只需要继承就可以了
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable<U> {
    @CreatedBy
    @Column(name = "created_by")
    private U createdBy;

    @CreatedDate
    @Column(name = "created_date")
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "update_by")
    private U updateBy;

    @LastModifiedDate
    @Column(name = "update_date")
    private Date updateDate;
}

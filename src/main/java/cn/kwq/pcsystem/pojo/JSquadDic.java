package cn.kwq.pcsystem.pojo;

import cn.kwq.pcsystem.pojo.Auditable.Auditable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/01/14:25
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity //作为hibernate
@Table(name = "squad_Dic")
@Builder
public class JSquadDic extends Auditable<Long> {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//id自增
    @Column(name = "squad_id")
    private Long squadId;

    private String squadName;

    private String remark;

    private String grade;

    private Long majorId;

    private Long departmentId;

    private int deleted;
}

package cn.kwq.pcsystem.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/23/16:55
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity //作为hibernate
@Table(name = "Application_Form")
@Builder
@Accessors(chain = true)
public class JApplicationForm {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//id自增
    @Column(name = "application_from_id")
    private Long ApplicationFromId;

    private int sex;

    private String ethnic;

    private Date born;

    private String idCard;

    private int accountType;

    private Long departmentId;

    private Long majorId;

    private Long squadId;

    private int familySize;

    private int familyStudentSize;

    private int familySupportSize;

    private int familyUnemployedSize;

    private int health;

    private String address;

    private String postalCode;

    private int avgYearIncome;

    private int incomeSource;

    private String emergency;

    private int emergencyType;

    private String emergencyDescribes;

    private String otherEmergency;

    private String stuNum;
    private String name;
    //学院
    private String facultyName;
    private String majorName;
    private String wyuClass;
    private String grade;
    //入学年份
    private Date enterYear;


}

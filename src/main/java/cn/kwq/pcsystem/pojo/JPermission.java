package cn.kwq.pcsystem.pojo;

import cn.kwq.pcsystem.pojo.Auditable.Auditable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/01/12/23:49
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity //作为hibernate
@Table(name = "Permission")
@Builder
public class JPermission extends Auditable<Long> {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//id自增
    @Column(name = "permission_id")
    private Long permissionId;

    private String permission;

    private String description;

    private String remark;

    private int deleted;


}

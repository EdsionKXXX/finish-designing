package cn.kwq.pcsystem.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/23/17:29
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity //作为hibernate
@Table(name = "family_status_dic")
@Builder
public class JFamilyStatusDic {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//id自增
    @Column(name = "familyStatus_id")
    private Long familyStatusId;

    private String FamilyStatusName;

    private String remark;

    private int deleted;
}

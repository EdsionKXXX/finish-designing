package cn.kwq.pcsystem.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/12/16:08
 * @Description:
 */
@Builder
@Entity //作为hibernate
@Table(name = "real_user")
@NoArgsConstructor
@AllArgsConstructor
public class JRealUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//id自增
    private Long RealUserId;

    private Long userId;

    private String realUserName;

    private String schoolNum;

    private Long departmentId;

    private int isStudent;

    private String remark;

    private int deleted;


}

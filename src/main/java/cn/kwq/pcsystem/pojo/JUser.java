package cn.kwq.pcsystem.pojo;

import cn.kwq.pcsystem.pojo.Auditable.Auditable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;


/**
 * Created with IntelliJ IDEA.
 * @Author: kwq
 * @Date: 2022/12/21/19:22
 * @Description:
 */
@Data
@Builder
@Entity //作为hibernate
@Table(name = "user")
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class JUser extends Auditable<Long> {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//id自增
    private Long userId;

    private String userName;

    private String password;

    private String salt;

    private int age;

    private String email;

    private String phone;

    private String avatarUrl;

    private int sex;

    private int deleted;



}

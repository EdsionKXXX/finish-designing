package cn.kwq.pcsystem.service;

import cn.kwq.pcsystem.dto.LoginDTO;
import cn.kwq.pcsystem.dto.RealUserDTO;
import cn.kwq.pcsystem.dto.RegisterDTO;
import cn.kwq.pcsystem.entity.User;
import cn.kwq.pcsystem.enums.RecoverEnum;
import cn.kwq.pcsystem.exception.CaptchaException;
import cn.kwq.pcsystem.exception.UserExistException;
import cn.kwq.pcsystem.exception.UserFormatException;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kwq
 * @since 2023-01-09
 */
public interface UserService extends IService<User> {
    boolean register(RegisterDTO dto) throws UserExistException, UserFormatException, CaptchaException;

    Long login(LoginDTO dto) throws UserExistException;

    User createUserNotThroughRegister(RealUserDTO dto);

    boolean updateUserPsw(String oldPsw, String newPsw , Long userId);

    void inactivationOrRecoverUser(RecoverEnum recoverEnum, Long userId);

    Map<String,String> loginWyuSys(String token) throws IOException, ParseException;
}

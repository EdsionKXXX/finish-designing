package cn.kwq.pcsystem.service;

import cn.kwq.pcsystem.entity.ApplicationForm;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kwq
 * @since 2023-03-23
 */
public interface ApplicationFormService extends IService<ApplicationForm> {

}

package cn.kwq.pcsystem.service;

import cn.kwq.pcsystem.entity.RealUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kwq
 * @since 2023-03-12
 */
public interface RealUserService extends IService<RealUser> {

}

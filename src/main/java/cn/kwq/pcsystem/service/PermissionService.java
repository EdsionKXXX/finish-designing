package cn.kwq.pcsystem.service;

import cn.kwq.pcsystem.entity.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kwq
 * @since 2023-01-12
 */
public interface PermissionService extends IService<Permission> {

}

package cn.kwq.pcsystem.service.impl;

import cn.kwq.pcsystem.entity.Role;
import cn.kwq.pcsystem.mapper.RoleMapper;
import cn.kwq.pcsystem.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kwq
 * @since 2023-01-12
 */
@Service
public class RoleServiceImp extends ServiceImpl<RoleMapper, Role> implements RoleService {

}

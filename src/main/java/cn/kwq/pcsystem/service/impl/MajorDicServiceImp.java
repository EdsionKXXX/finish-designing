package cn.kwq.pcsystem.service.impl;

import cn.kwq.pcsystem.entity.MajorDic;
import cn.kwq.pcsystem.mapper.MajorDicMapper;
import cn.kwq.pcsystem.service.MajorDicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kwq
 * @since 2023-03-12
 */
@Service
public class MajorDicServiceImp extends ServiceImpl<MajorDicMapper, MajorDic> implements MajorDicService {

}

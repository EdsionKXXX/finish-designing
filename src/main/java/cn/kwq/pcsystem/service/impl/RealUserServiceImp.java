package cn.kwq.pcsystem.service.impl;

import cn.kwq.pcsystem.entity.RealUser;
import cn.kwq.pcsystem.mapper.RealUserMapper;
import cn.kwq.pcsystem.service.RealUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kwq
 * @since 2023-03-12
 */
@Service
public class RealUserServiceImp extends ServiceImpl<RealUserMapper, RealUser> implements RealUserService {

}

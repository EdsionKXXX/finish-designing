package cn.kwq.pcsystem.service.impl;

import cn.kwq.pcsystem.entity.SquadDic;
import cn.kwq.pcsystem.mapper.SquadDicMapper;
import cn.kwq.pcsystem.service.SquadDicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kwq
 * @since 2023-03-12
 */
@Service
public class SquadDicServiceImp extends ServiceImpl<SquadDicMapper, SquadDic> implements SquadDicService {

}

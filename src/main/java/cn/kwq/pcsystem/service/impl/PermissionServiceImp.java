package cn.kwq.pcsystem.service.impl;

import cn.kwq.pcsystem.entity.Permission;
import cn.kwq.pcsystem.mapper.PermissionMapper;
import cn.kwq.pcsystem.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kwq
 * @since 2023-01-12
 */
@Service
public class PermissionServiceImp extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

}

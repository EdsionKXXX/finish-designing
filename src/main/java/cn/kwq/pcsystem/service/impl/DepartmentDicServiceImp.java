package cn.kwq.pcsystem.service.impl;

import cn.kwq.pcsystem.entity.DepartmentDic;
import cn.kwq.pcsystem.mapper.DepartmentDicMapper;
import cn.kwq.pcsystem.service.DepartmentDicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kwq
 * @since 2023-03-12
 */
@Service
public class DepartmentDicServiceImp extends ServiceImpl<DepartmentDicMapper, DepartmentDic> implements DepartmentDicService {

}

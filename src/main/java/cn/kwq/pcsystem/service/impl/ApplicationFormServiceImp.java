package cn.kwq.pcsystem.service.impl;

import cn.kwq.pcsystem.entity.ApplicationForm;
import cn.kwq.pcsystem.mapper.ApplicationFormMapper;
import cn.kwq.pcsystem.service.ApplicationFormService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kwq
 * @since 2023-03-23
 */
@Service
public class ApplicationFormServiceImp extends ServiceImpl<ApplicationFormMapper, ApplicationForm> implements ApplicationFormService {

}

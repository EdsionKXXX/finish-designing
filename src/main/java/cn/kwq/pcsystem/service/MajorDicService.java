package cn.kwq.pcsystem.service;

import cn.kwq.pcsystem.entity.MajorDic;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kwq
 * @since 2023-03-12
 */

public interface MajorDicService extends IService<MajorDic> {

}

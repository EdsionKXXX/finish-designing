package cn.kwq.pcsystem.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author kwq
 * @since 2023-03-23
 */
@Getter
@Setter
@TableName("family_status_dic")
@ApiModel(value = "FamilyStatusDic对象", description = "")
public class FamilyStatusDic implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "family_status_id", type = IdType.AUTO)
    private Long familyStatusId;

    private String familyStatusName;

    private Integer deleted;

    private String remark;


}

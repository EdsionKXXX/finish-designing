package cn.kwq.pcsystem.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author kwq
 * @since 2023-01-09
 * mp实体类，查询用
 */
@Getter
@Setter
@Builder
@ApiModel(value = "User对象", description = "")
@TableName("user")
@Accessors(chain = true)
@ToString
public class User implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    private String userName;

    private String password;

    private String salt;

    private Integer age;

    private String avatarUrl;

    private String email;

    private String phone;

    private Integer sex;

    private Long createdBy;

    private Date createdDate;

    private Long updateBy;

    private Date updateDate;

    @TableLogic
    private int deleted;


}

package cn.kwq.pcsystem.entity;

import cn.kwq.pcsystem.enums.DepartmentEnum;
import cn.kwq.pcsystem.enums.StuEnum;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author kwq
 * @since 2023-03-12
 */
@Getter
@Setter
@TableName("real_user")
@ApiModel(value = "RealUser对象", description = "")
public class RealUser implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @TableId(value = "real_user_id", type = IdType.AUTO)
    private Long realUserId;

    private Integer deleted;

    private DepartmentEnum department;

    private StuEnum isStudent;

    private String realUserName;

    private String schoolNum;

    private String remark;

    private Long userId;


}

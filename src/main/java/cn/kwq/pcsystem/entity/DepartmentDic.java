package cn.kwq.pcsystem.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 
 * </p>
 *
 * @author kwq
 * @since 2023-03-12
 */
@Getter
@Setter
@TableName("department_dic")
@ApiModel(value = "DepartmentDic对象", description = "")
@Builder
@ToString
public class DepartmentDic implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @TableId(value = "department_id", type = IdType.AUTO)
    private Long departmentId;

    private Long createdBy;

    private Date createdDate;

    private Long updateBy;

    private Date updateDate;

    private Integer deleted;

    private String departmentName;

    private String enumName;

    private String remark;


}

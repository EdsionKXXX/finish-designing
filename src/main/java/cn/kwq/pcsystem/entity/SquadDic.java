package cn.kwq.pcsystem.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author kwq
 * @since 2023-03-12
 */
@Getter
@Setter
@TableName("squad_dic")
@ApiModel(value = "SquadDic对象", description = "")
public class SquadDic implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @TableId(value = "squad_id", type = IdType.AUTO)
    private Long squadId;

    private Long createdBy;

    private Date createdDate;

    private Long updateBy;

    private Date updateDate;

    private Integer deleted;

    private Long departmentId;

    private String grade;

    private Long majorId;

    private String remark;

    private String squadName;


}

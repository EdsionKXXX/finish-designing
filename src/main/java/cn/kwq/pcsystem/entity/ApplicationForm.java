package cn.kwq.pcsystem.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author kwq
 * @since 2023-03-23
 */
@Getter
@Setter
@TableName("application_form")
@ApiModel(value = "ApplicationForm对象", description = "")
public class ApplicationForm implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "application_from_id", type = IdType.AUTO)
    private Long applicationFromId;

    private Integer accountType;

    private String address;

    private Integer avgYearIncome;

    private Date born;

    private Long departmentId;

    private String emergency;

    private String emergencyDescribes;

    private Integer emergencyType;

    private Date enterYear;

    private String ethnic;

    private String facultyName;

    private Integer familySize;

    private Integer familyStudentSize;

    private Integer familySupportSize;

    private Integer familyUnemployedSize;

    private String grade;

    private Integer health;

    private String idCard;

    private Integer incomeSource;

    private Long majorId;

    private String majorName;

    private String name;

    private String otherEmergency;

    private String postalCode;

    private Integer sex;

    private Long squadId;

    private String stuNum;

    private String wyuClass;


}

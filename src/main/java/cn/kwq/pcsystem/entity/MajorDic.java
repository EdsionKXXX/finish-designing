package cn.kwq.pcsystem.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author kwq
 * @since 2023-03-12
 */
@Getter
@Setter
@TableName("major_dic")
@ApiModel(value = "MajorDic对象", description = "")
@Builder
public class MajorDic implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @TableId(value = "major_id", type = IdType.AUTO)
    private Long majorId;

    private Integer deleted;

    private Long departmentId;

    private String majorName;

    private String remark;


}

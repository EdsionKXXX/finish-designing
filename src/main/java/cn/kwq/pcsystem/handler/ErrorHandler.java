package cn.kwq.pcsystem.handler;

import cn.dev33.satoken.exception.NotLoginException;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONException;

import cn.kwq.pcsystem.common.Result;
import cn.kwq.pcsystem.common.ResultCode;
import cn.kwq.pcsystem.exception.CaptchaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

@ControllerAdvice
@Slf4j
public class ErrorHandler {
    /**
     * 全局异常拦截（拦截项目中的NotLoginException异常）
     */
    @ResponseBody
    @ExceptionHandler(value = NotLoginException.class)
    public Result handlerNotLoginException(HttpServletRequest httpServletRequest, NotLoginException nle) {

        System.out.println("=======================================出现账户错误============================================");
        System.out.println("==================================="+ DateUtil.format(new Date(),"yyyy/MM/dd HH:mm:ss") +"========================================");
        log.error("[err msg]"+nle.getMessage());
        Optional.ofNullable(nle.getCause().getMessage()).ifPresent(cause-> log.error("[err cause]"+cause));
        // 打印堆栈，以供调试
        nle.printStackTrace();

        try {
            if(nle.getType().equals(NotLoginException.NOT_TOKEN)) {
                return Result.error(ResultCode.USER_NOT_LOGIN);
            }
            else if(nle.getType().equals(NotLoginException.INVALID_TOKEN)) {
                return Result.error(ResultCode.USER_ACCOUNT_EXPIRED);
            }
            else if(nle.getType().equals(NotLoginException.TOKEN_TIMEOUT)) {
                return Result.error(ResultCode.USER_CREDENTIALS_EXPIRED);
            }
            else if(nle.getType().equals(NotLoginException.BE_REPLACED)) {
                //无状态token不可实现
                return Result.error(ResultCode.USER_ACCOUNT_USE_BY_OTHERS);
            }
            else if(nle.getType().equals(NotLoginException.KICK_OUT)) {
                //无状态token不可实现
                return Result.error(ResultCode.USER_ACCOUNT_USE_BY_OTHERS);
            }
            else {
                return Result.error(ResultCode.USER_NOT_LOGIN);
            }
        }finally {
            System.out.println("========================================== EOF ===============================================");
        }
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public Result handlerAllException(HttpServletRequest httpServletRequest, Exception err) {
        System.out.println("=======================================出现业务错误============================================");
        System.out.println("==================================="+ DateUtil.format(new Date(),"yyyy/MM/dd HH:mm:ss") +"========================================");
        log.error("[err msg]"+err.getMessage());
        Optional.ofNullable(err.getCause()).ifPresent(cause-> log.error("[err cause]"+cause));
        //打印错误堆栈
        err.printStackTrace();
        try {
            if (err instanceof cn.kwq.pcsystem.exception.CaptchaException){
                //验证码错误
                return Result.error(ResultCode.CAPTCHA_ERROR);
            } else if (err instanceof cn.kwq.pcsystem.exception.UserExistException) {
                //账号存在业务错误
                return Result
                        .error(ResultCode.USER_ACCOUNT_EXIST)
                        .msg(err.getMessage());
            } else if (err instanceof cn.kwq.pcsystem.exception.UserFormatException) {
                //账号格式业务错误
                return Result
                        .error(ResultCode.USER_FORMAT_ERROR)
                        .msg(err.getMessage());
            } else if ("密码错误".equals(err.getMessage())){
                //密码错误
                return Result
                        .error(ResultCode.USER_CREDENTIALS_ERROR);
            }

        }finally {
            System.out.println("========================================== EOF ===============================================");
        }
        return Result.error();
    }




    //错误为aop产生的时候进入
//        if (err instanceof java.lang.reflect.UndeclaredThrowableException){
//            //判断注解种类(利用反射)
//            if (err.getCause() instanceof RequestLimitException){
//                return E.err(4100,nle.getCause().getMessage());
//            }
//        }

}

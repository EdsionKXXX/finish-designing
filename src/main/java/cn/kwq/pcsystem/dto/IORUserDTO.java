package cn.kwq.pcsystem.dto;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/20/15:41
 * @Description:
 */
@Data
public class IORUserDTO {
    private String type;
    private Long userId;
}

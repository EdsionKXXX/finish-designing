package cn.kwq.pcsystem.dto;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/01/06/19:24
 * @Description: jdk新特性record，单纯程序内部用来交互数据直接用record
 */
public record WyuDTO(
        String stuNum,
        String name,
        //学院
        String institute,
        String major,
        String wyuClass,
        String grade,
        //入学年份
        Date year
) {
}

package cn.kwq.pcsystem.dto;

import cn.kwq.pcsystem.pojo.JFamily;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/23/18:34
 * @Description:
 */
@Data
public class ApplyDTO implements Serializable {

    private Long ApplicationFromId;

    private int sex;

    private String ethnic;

    private Date born;

    private String idCard;

    private int accountType;

    private int familySize;

    private int familyStudentSize;

    private int familySupportSize;

    private int familyUnemployedSize;

    private int health;

    private String address;

    private String postalCode;

    private int avgYearIncome;

    private int incomeSource;

    private String emergency;

    private int emergencyType;

    private String emergencyDescribes;

    private String otherEmergency;

    private List<JFamily> families;

    private List<Long> familyStatus;
}

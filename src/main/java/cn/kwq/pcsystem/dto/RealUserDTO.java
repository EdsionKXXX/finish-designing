package cn.kwq.pcsystem.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/14/15:45
 * @Description:
 */
@Data
public class RealUserDTO {

    @ExcelProperty("用户姓名（自定义的名字）")
    private String userName;

    @ExcelProperty("部门代号")
    private Long departmentId;

    @ExcelProperty("是否学生（1是0不是）")
    private Integer isStudent;

    @ExcelProperty("真实姓名")
    private String realUserName;

    @ExcelProperty("学校的凭证（学号/教职工号）")
    private String schoolNum;

    @ExcelProperty("备注")
    private String remark;

    @ExcelProperty("手机")
    private String phone;

    @ExcelProperty("邮箱")
    private String email;
}

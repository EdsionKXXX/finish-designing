package cn.kwq.pcsystem.dto;
/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/01/06/19:24
 * @Description: jdk新特性record，单纯程序内部用来交互数据直接用record
 */
public record Md5DTO(
        //加密后密文
        String encrypt,
        //加密后盐
        String salt
) {
}

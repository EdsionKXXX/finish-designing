package cn.kwq.pcsystem.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/23/20:48
 * @Description:
 */
@Data
public class LoginWyuDTO {

    @ApiModelProperty(value = "登录教务系统返回的Cookie")
    private String token;
    @ApiModelProperty(value = "登录微信返回的code")
    private String code;

}

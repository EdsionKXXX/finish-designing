package cn.kwq.pcsystem.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * 注册传输数据
 * @Author: kwq
 * @Date: 2023/01/10/0:45
 * @Description:
 */
@Data
public class RegisterDTO implements Serializable {
    /*
    * 表单信息
    */
    private String userName;

    private String password;

    private Integer age;

    private String email;

    private String phone;

    private Integer sex;

    /*
     * 验证码信息
     */
    private String uuid;

    private String captcha;
}

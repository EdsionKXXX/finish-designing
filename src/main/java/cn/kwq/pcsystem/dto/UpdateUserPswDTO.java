package cn.kwq.pcsystem.dto;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/20/15:29
 * @Description:
 */
@Data
public class UpdateUserPswDTO {
    String oldPsw;
    String newPsw;
}

package cn.kwq.pcsystem.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/19/22:28
 * @Description:
 */
@Data
public class SquadExcelDTO {

    @ExcelProperty("班级")
    private String squadName;
    @ExcelProperty("年级")
    private String grade;
    @ExcelProperty("专业代号")
    private Long majorId;
    @ExcelProperty("部门代号")
    private Long departmentId;
    @ExcelProperty("备注")
    private String remark;

}

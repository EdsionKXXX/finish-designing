package cn.kwq.pcsystem.dto;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/02/04/15:11
 * @Description:
 */
@Data
public class LoginDTO {

    //可以是名字 可以是邮箱 可以是手机
    private String loginKey;

    private String password;
}

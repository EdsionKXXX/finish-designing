package cn.kwq.pcsystem;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;


/**
 * @author kwq
 */
@SpringBootApplication
@EnableTransactionManagement // 开启事务支持
@EnableJpaAuditing//开启审计
@Slf4j
@EnableAsync//开启异步工作
public class PovertyCertificationSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(PovertyCertificationSystemApplication.class, args);
        log.warn("查询请用mp"+",更新和插入请用jpa");
    }



}

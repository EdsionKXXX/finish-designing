package cn.kwq.pcsystem.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/14/17:20
 * @Description:
 */
@Data
@Builder
public class DepartmentDicVO {

    @ExcelProperty("序号")
    private int num;

    @ExcelProperty("部门/学院")
    private String departmentName;

    @ExcelProperty("系统内代号")
    private Long departmentId;

    @ExcelProperty("备注")
    private String remark;
}

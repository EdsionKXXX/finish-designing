package cn.kwq.pcsystem.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: kwq
 * @Date: 2023/03/15/10:15
 * @Description:
 */
@Data
@Builder
public class MajorDicVO {

    @ExcelProperty("序号")
    private int num;

    @ExcelProperty("专业名")
    private String majorName;

    @ExcelProperty("系统内代号")
    private Long majorId;

    @ExcelProperty("备注")
    private String remark;
}

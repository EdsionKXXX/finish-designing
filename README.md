# 毕设

#### 介绍

- 毕设目前在云端的一个存版
- 目前完成基础资料和相关架构的搭建
-  **以及需要用到的工具类搭建
** 

#### 软件架构
软件架构说明

-  **目前架构是springboot+docker的架构，后续考虑更换SpringBoot3（担心相关依赖无法适配）或者quarkus进行云原生相关的转换** 
- 持久化：mysql；
- 区块链信息存取：mongodb；
- 常用中间件：redis；
- 总体架构：springboot；
- 前端架构：uniapp+vue；
- orm架构：jpa+querydsl进行插入和更新，mybatis/mybatis—plus进行查询，redis和mongodb使用spring data；


#### 基础信息
项目概况:  使用区块链技术，构建一个透明的困难学生鉴定系统作为毕业设计

主要工作: 架构搭建，区块链系统搭建，docker 部署，前后端开发

技术概况: 

1. 根据区块链相关概念，构建区块链模型，使用 mongodb 存储链数据以实现区块链高qps的操作，mysql 持久化其他数据。
2. 使用redis 作为简单MQ用于mysql和区块链数据同步，以及Rust模块和Java代码之间的分布式锁和分布式缓存，且实现了幂等性校验，防盗刷等业务功能。
3. 使用uniapp＋vue 构建前端小程序，orm 选用取 querysql+mybatis+jpa 结合开发，并使用Java 爬取学校相关数据，以证明验证数据真实性。



#### 安装教程

1.  区块链架构已经基于docker部署完成
2.  可以通过docker部署
3.  可以通过k8s部署
4.  后续可能会转换springboot3 进行 graalvm的编译

#### 使用说明

1.  运行环境：centos+docker/k8s


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
